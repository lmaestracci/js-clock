const secondeDiv = document.querySelector('.seconde')
const minDiv = document.querySelector('.min')
const heureDiv = document.querySelector('.heure')


function miseEnPlace(){
    const now = new Date();

    const seconds = now.getSeconds();
    const secDeg = ((seconds/60)*360) + 90;
    secondeDiv.style.transform = `rotate(${secDeg}deg)`;

    const mins = now.getMinutes();
    const minDeg = ((mins/60)*360) + ((seconds/60)*6) + 90;
    minDiv.style.transform = `rotate(${minDeg}deg)`;

    const hour = now.getHours();
    const hourDeg = ((hour/12)*360) + ((mins/60)*30) + 90;
    heureDiv.style.transform = `rotate(${hourDeg}deg)`;
}

setInterval(miseEnPlace, 1000);